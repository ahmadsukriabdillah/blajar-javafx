package com.client.blajar.Repository;

import com.client.blajar.Constanta;
import com.client.blajar.Controller.HomeController;
import com.client.blajar.Helper.Dlg;
import com.client.blajar.Model.Pelanggan;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sukri on 07/12/16.
 */

@Async
@Service
public class PelangganRestRespository {
    private static final Logger logger = LoggerFactory.getLogger(PelangganRestRespository.class);

    private final RestTemplate restTemplate;

    public PelangganRestRespository(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.setConnectTimeout(2000).build();
        restTemplate.setRequestFactory(clientHttpRequestFactory());
    }

    public Pelanggan find(String id){
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.add("Authorization", HomeController.get_token());
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
            ResponseEntity<Pelanggan> result = restTemplate.exchange(Constanta.REST_PELANGGAN+id, HttpMethod.GET, entity, Pelanggan.class);
            logger.debug(result.toString());
            return result.getBody();
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode().value()){
                case 401:
                    Dlg.eror("Error","Session Expired");
                    break;

            }
            return null;
        }catch (RestClientException e){
            return null;
        }

    }

    private ClientHttpRequestFactory clientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setReadTimeout(2000);
        factory.setConnectTimeout(2000);
        return factory;
    }

    private String findAll(){
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.add("Authorization", HomeController.get_token());
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
            ResponseEntity<String> result = restTemplate.exchange(Constanta.REST_PELANGGAN, HttpMethod.GET, entity, String.class);
            logger.debug(result.getBody());
            return result.getBody();
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode().value()){
                case 401:
                    Dlg.eror("Error","Session Expired");
                    break;

            }
            return null;
        }catch (RestClientException e){
            return null;
        }

    }

    public List<Pelanggan> findAllUser(){
        String sers = findAll();
        ObjectMapper mapper = new ObjectMapper();

        List<Pelanggan> items = null;
        try {
            items = mapper.readValue(sers, new TypeReference<List<Pelanggan>>(){});
            return items;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    @Async
    public void createPelanggan(Pelanggan pelanggan){
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", HomeController.get_token());
            HttpEntity<Pelanggan> req = new HttpEntity<>(pelanggan,headers);
            Pelanggan u = restTemplate.postForObject(Constanta.REST_PELANGGAN,req,Pelanggan.class);
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode().value()){
                case 401:
                    Dlg.eror("Error","Session Expired");
                    break;

            }

        }catch (RestClientException e){
        }
    }

    @Async
    public void deletePelanggan(Pelanggan pelanggan){
        try {
            String url = Constanta.REST_PELANGGAN+pelanggan.getId();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", HomeController.get_token());
            HttpEntity<String> req = new HttpEntity<>(headers);
            ResponseEntity<String> a = restTemplate.exchange(url,HttpMethod.DELETE,req,String.class);
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode().value()){
                case 401:
                    Dlg.eror("Error","Session Expired");
                    break;

            }
        }catch (RestClientException e){
        }
    }

    @Async
    public void updatePelanggan(String id,Pelanggan pelanggan){
        try {
            pelanggan.setId(null);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", HomeController.get_token());
            HttpEntity<Pelanggan> req = new HttpEntity<>(pelanggan,headers);
            String resourceUrl = Constanta.REST_PELANGGAN + id;
            restTemplate.put(resourceUrl,req,Pelanggan.class);
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode().value()){
                case 401:
                    Dlg.eror("Error","Session Expired");
                    break;
            }
        }catch (RestClientException e){

        }

    }






}


