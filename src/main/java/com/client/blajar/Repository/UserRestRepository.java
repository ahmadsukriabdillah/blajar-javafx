package com.client.blajar.Repository;

import com.client.blajar.Constanta;
import com.client.blajar.Controller.HomeController;
import com.client.blajar.Helper.Dlg;
import com.client.blajar.Model.User;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sukri on 07/12/16.
 */

@Async
@Service
public class UserRestRepository {
    private static final Logger logger = LoggerFactory.getLogger(UserRestRepository.class);

    private final RestTemplate restTemplate;

    public UserRestRepository(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }


    public User find(String id){
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.add("Authorization", HomeController.get_token());
            HttpEntity<String> entity = new HttpEntity<String>(null, headers);
            ResponseEntity<User> result = restTemplate.exchange(Constanta.REST_PEGAWAI+id, HttpMethod.GET, entity, User.class);
            logger.debug(result.toString());
            return result.getBody();
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }

    }
    private String findAll(){
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.add("Authorization", HomeController.get_token());
            HttpEntity<String> entity = new HttpEntity<String>(null, headers);
            ResponseEntity<String> result = restTemplate.exchange(Constanta.REST_PEGAWAI, HttpMethod.GET, entity, String.class);
            logger.debug(result.getBody());
            return result.getBody();
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode().value()){
                case 401:
                    Dlg.eror("Error","Session Expired");
                    break;

            }
            return null;
        }catch (RestClientException e){
            return null;
        }

    }

    public List<User> findAllUser(){
        String sers = findAll();
        ObjectMapper mapper = new ObjectMapper();

        List<User> items = null;
        try {
            items = mapper.readValue(sers, new TypeReference<List<User>>(){});
            return items;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void createUser(User user){
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", HomeController.get_token());

            HttpEntity<?> req = new HttpEntity<>(user,headers);
            ResponseEntity<User> u = restTemplate.exchange(Constanta.REST_PEGAWAI,HttpMethod.POST,req,User.class);
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode().value()){
                case 401:
                    Dlg.eror("Error","Session Expired");
                    break;

            }
        }catch (RestClientException e){
        }
    }
    public void updateUser(String id,User user){
        try {
            user.setId(null);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", HomeController.get_token());

            HttpEntity<?> req = new HttpEntity<>(user,headers);
            ResponseEntity<User> u = restTemplate.exchange(Constanta.REST_PEGAWAI+id,HttpMethod.PUT,req,User.class);
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode().value()){
                case 401:
                    Dlg.eror("Error","Session Expired");
                    break;

            }
        }catch (RestClientException e){

        }
    }

    public void deleteUser(String id){
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", HomeController.get_token());

            HttpEntity<?> req = new HttpEntity<>(headers);
            ResponseEntity<User> u = restTemplate.exchange(Constanta.REST_PEGAWAI+id,HttpMethod.DELETE,req,User.class);
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode().value()){
                case 401:
                    Dlg.eror("Error","Session Expired");
                    break;

            }
        }catch (RestClientException e){
        }
    }

}
