package com.client.blajar.Controller.Pelanggan;

import com.client.blajar.Controller.BootInitIalizer;
import com.client.blajar.Helper.Dlg;
import com.client.blajar.Controller.HomeController;
import com.client.blajar.Model.Pelanggan;
import com.client.blajar.Repository.PelangganRestRespository;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by sukri on 01/12/16.
 */
@Component
public class ListPelangganController implements BootInitIalizer {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    public Button update;
    public Button delete;
    @Autowired
    ListPelangganController listPelangganController;
    @FXML
    private TextField tanggal;
    @FXML
    private TextField pekerjaan;
    @FXML
    private TextField nama;
    @FXML
    private TextField tempatlahir;
    @FXML
    private TextField penghasilan;
    @FXML
    private TextField alamat;
    @FXML
    private TableView<Pelanggan> tableview;
    @FXML
    private TableColumn<Pelanggan,String> tableid;
    @FXML
    private TableColumn<Pelanggan,String> tablename;
    @FXML
    private TableColumn<Pelanggan,String> tablepekerajaan;
    @FXML
    private TableColumn<Pelanggan,String> tablepenghasilan;

    private ApplicationContext spring;

    @Autowired
    private HomeController homeController;
    private  Pelanggan pelanggan;

    @Autowired
    private PelangganRestRespository pelangganRestRespository;
    @Autowired
    private FormPelangganController formPelangganController;

    @Override
    public void initConstruct() {

        reload_table();
    }

    private void reload_table() {

        tableview.getItems().clear();
        tableview.getItems().addAll(pelangganRestRespository.findAllUser());
    }

    @Override
    public void stage(Stage primaryStage) {

    }

    @Override
    public Node InitView() {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/scene/pelanggan/list.fxml"));
            loader.setController(spring.getBean(this.getClass()));
            return loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tablename.setCellValueFactory(new PropertyValueFactory<Pelanggan, String>("nama"));
        tablepekerajaan.setCellValueFactory(new PropertyValueFactory<Pelanggan, String>("pekerjaan"));
        tablepenghasilan.setCellValueFactory(new PropertyValueFactory<Pelanggan, String>("penghasilan"));
        tableid.setCellValueFactory(new PropertyValueFactory<Pelanggan, String>("id"));
        tableview.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Pelanggan> val,Pelanggan oldValue,Pelanggan newValue) -> {
            update.setDisable(newValue == null);
            update.setOnAction(event -> {
                doUpdate(event,newValue);
            });
            delete.setDisable(newValue == null);
            delete.setOnAction(event -> {
                doDelete(event,newValue);
            });

            this.pelanggan = newValue;

            tanggal.setText(newValue.getTglLAHIR());
            pekerjaan.setText(newValue.getPekerjaan());
            penghasilan.setText(newValue.getPenghasilan().toString());
            nama.setText(newValue.getNama());
            tempatlahir.setText(newValue.getTempat());
            alamat.setText(newValue.getAlamat());
        });
    }

    private void doDelete(ActionEvent event, Pelanggan newValue) {

        if(Dlg.show("Peingatan","Apa anda yakin untuk menghapus data ini")){
            pelangganRestRespository.deletePelanggan(newValue);
            tableview.getItems().remove(newValue);
        }


    }

    private void doUpdate(ActionEvent e,Pelanggan newValue) {
        homeController.setCenterLayout(formPelangganController.InitView());
        formPelangganController.initConstruct(newValue);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.spring = applicationContext;

    }

    public void tambah(ActionEvent actionEvent) {
        homeController.setCenterLayout(formPelangganController.InitView());
        formPelangganController.initConstruct();

    }

    public void update(ActionEvent actionEvent) {

    }

    public void delete(ActionEvent actionEvent) {

    }
}
