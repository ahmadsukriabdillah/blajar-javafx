package com.client.blajar.Controller.Pelanggan;

import com.client.blajar.Controller.BootInitIalizer;
import com.client.blajar.Helper.Dlg;
import com.client.blajar.Controller.HomeController;
import com.client.blajar.Model.Pelanggan;
import com.client.blajar.Repository.PelangganRestRespository;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by sukri on 01/12/16.
 */
@Component
public class FormPelangganController implements BootInitIalizer {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    private ApplicationContext spring;


    @Autowired
    private ListPelangganController listPelangganController;

    @Autowired
    private HomeController homeController;
    @Autowired
    private PelangganRestRespository pelangganRestRespository;
    private Pelanggan pelanggan;
    private boolean edited;

    @FXML
    private TextField tanggal;
    @FXML
    private TextField pekerjaan;
    @FXML
    private TextField nama;
    @FXML
    private TextField tempatlahir;
    @FXML
    private TextField penghasilan;
    @FXML
    private TextField alamat;

    public boolean isEdited() {
        return edited;
    }

    public void setEdited(boolean edited) {
        this.edited = edited;
    }

    @Override
    public void initConstruct()
    {
        clear();
        setEdited(false);
    }
    public void initConstruct(Pelanggan newValue) {
        clear();
        setEdited(true);
        pelanggan = newValue;
        tanggal.setText(newValue.getTglLAHIR());
        pekerjaan.setText(newValue.getPekerjaan());
        penghasilan.setText(newValue.getPenghasilan().toString());
        nama.setText(newValue.getNama());
        tempatlahir.setText(newValue.getTempat());
        alamat.setText(newValue.getAlamat());



    }

    private void clear(){
        tanggal.clear();
        pekerjaan.clear();
        penghasilan.clear();
        nama.clear();
        tempatlahir.clear();
        alamat.clear();
    }


    @Override
    public void stage(Stage primaryStage) {

    }

    @Override
    public Node InitView() {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/scene/pelanggan/form.fxml"));
            loader.setController(spring.getBean(this.getClass()));
            return loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.spring = applicationContext;

    }

    

    public void doSave(ActionEvent actionEvent) {
        if(isEdited()){
            if(Dlg.show("Konfirmasi","Apa yakin untuk mengubah data?")){
                ProgressBar progressBar = new ProgressBar();
                Task a = new Task() {
                    @Override
                    protected Object call() throws Exception {
                        pelanggan.setAlamat(alamat.getText());
                        pelanggan.setNama(nama.getText());
                        pelanggan.setPekerjaan(pekerjaan.getText());
                        pelanggan.setTglLAHIR(tanggal.getText());
                        pelanggan.setPenghasilan(Integer.parseInt(penghasilan.getText()));
                        pelanggan.setTempat(tempatlahir.getText());
                        pelangganRestRespository.updatePelanggan(pelanggan.getId().toString(),pelanggan);
                        return null;
                    }
                };
                a.setOnRunning(event -> {
                    progressBar.setVisible(true);
                });

                a.setOnSucceeded(event -> {
                    progressBar.setVisible(false);
                    doCance(actionEvent);
                });
                a.run();

            }

        }else{
            if(Dlg.show("Konfirmasi","Apa yakin untuk mengubah data?")) {
                Pelanggan a = new Pelanggan();
                a.setAlamat(alamat.getText());
                a.setNama(nama.getText());
                a.setPekerjaan(pekerjaan.getText());
                a.setTglLAHIR(tanggal.getText());
                a.setPenghasilan(Integer.parseInt(penghasilan.getText()));
                a.setTempat(tempatlahir.getText());
                pelangganRestRespository.createPelanggan(a);
                doCance(actionEvent);
            }


        }
    }

    public void doCance(ActionEvent actionEvent) {
        homeController.setCenterLayout(listPelangganController.InitView());
        listPelangganController.initConstruct();
    }
}
