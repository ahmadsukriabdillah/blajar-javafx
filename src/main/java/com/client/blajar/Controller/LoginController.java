package com.client.blajar.Controller;

import com.client.blajar.Constanta;
import com.client.blajar.Controller.Pelanggan.ListPelangganController;
import com.client.blajar.Helper.Dlg;
import com.client.blajar.Model.sercurity.Auth;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


/**
 * Created by sukri on 08/12/16.
 */

@Component
public class LoginController implements BootInitIalizer {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @FXML
    public PasswordField password;
    @FXML
    public TextField username;
    ApplicationContext applicationContext;
    @Autowired
    HomeController homeController;
    @Autowired
    ListPelangganController listPelangganController;

    @Override
    public void initConstruct() {
        username.clear();
        password.clear();
    }

    @Override
    public void stage(Stage primaryStage) {

    }

    @Override
    public Node InitView() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/scene/login.fxml"));
            loader.setController(applicationContext.getBean(this.getClass()));
            return loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {


    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public void login(ActionEvent actionEvent) {

        if(!username.getText().isEmpty() && !password.getText().isEmpty()){

            RestTemplate restTemplate = new RestTemplate();
            Auth auth = new Auth(username.getText(),password.getText());
            HttpEntity<?> req = new HttpEntity<>(auth);
            ResponseEntity<String> responseEntity = null;
            try{
                responseEntity = restTemplate.exchange(Constanta.AUTH, HttpMethod.POST,req,String.class);
                if(responseEntity.getStatusCode() == HttpStatus.UNAUTHORIZED){

                }else if(responseEntity.getStatusCode() == HttpStatus.OK){
                    JSONObject jsonObject = new JSONObject(responseEntity.getBody());
                    HomeController.set_token(jsonObject.getString("token"));
                    homeController.enableMenu(false);
                    homeController.setCenterLayout(listPelangganController.InitView());
                    listPelangganController.initConstruct();
                    logger.info(HomeController.get_token());
                }else{

                }

            }catch (HttpClientErrorException e){
                switch (e.getStatusCode().value()){
                    case 401:
                        password.clear();
                        password.requestFocus();
                        Dlg.eror("Erorr","Username / Passwrd salah");
                        break;

                }
                logger.info(e.toString());



            }

        }
    }
}
