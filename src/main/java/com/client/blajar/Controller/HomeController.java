package com.client.blajar.Controller;

import com.client.blajar.Controller.User.FormUserController;
import com.client.blajar.Controller.User.ListUserController;
import com.client.blajar.Helper.Dlg;
import com.client.blajar.Controller.Pelanggan.FormPelangganController;
import com.client.blajar.Controller.Pelanggan.ListPelangganController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by sukri on 01/12/16.
 */
@Component
public class HomeController implements BootInitIalizer {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    public BorderPane layoutHome;

    public MenuBar menu;
    private ApplicationContext spring;
    private static String  _token;

    public static String get_token() {
        return _token;
    }

    public void enableMenu(boolean e){
        menu.setDisable(e);
    }

    public static void set_token(String _token) {

        HomeController._token = _token;
    }

    @Autowired
    private FormPelangganController formPelangganController;
    @Autowired
    private ListPelangganController listPelangganController;

    @Autowired
    private FormUserController formUserController;
    @Autowired
    private ListUserController listUserController;

    @Autowired
    LoginController loginController;
    @Override
    public void initConstruct() {

        setCenterLayout(loginController.InitView());
        loginController.initConstruct();

    }
    
    @Override
    public void stage(Stage primaryStage) {

    }

    @Override
    public Node InitView() {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/scene/home.fxml"));
            loader.setController(spring.getBean(this.getClass()));
            return loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.spring = applicationContext;

    }

    public void doClose(ActionEvent actionEvent) {
        Platform.exit();
    }

    public void seePelanggan(ActionEvent actionEvent) {
        setCenterLayout(listPelangganController.InitView());
        listPelangganController.initConstruct();

    }

    public void doAbout(ActionEvent actionEvent) {


    }


    public void close(ActionEvent actionEvent) {
        if(Dlg.show("Warning","anda yakin untuk keluar dari sistem?")){
            setCenterLayout(loginController.InitView());
            loginController.initConstruct();
            enableMenu(true);
        }else{

        }

    }
    

    public void setCenterLayout(Node centerLayout) {
        this.layoutHome.setCenter(centerLayout);
        this.layoutHome.autosize();

    }

    public void addPelanggan(ActionEvent actionEvent) {
        setCenterLayout(formPelangganController.InitView());
        formPelangganController.initConstruct();
    }

    public void addUser(ActionEvent actionEvent) {
        setCenterLayout(formUserController.InitView());
        formUserController.initConstruct();
    }

    public void seeUser(ActionEvent actionEvent) {
        setCenterLayout(listUserController.InitView());
        listUserController.initConstruct();
//        Stage stage = new Stage();
//        ListPelangganController controller = spring.getBean(ListPelangganController.class);
//        Parent parent = (Parent) controller.InitView();
//        controller.initConstruct();
//        Scene scene = new Scene(parent);
//        stage.setResizable(false);
//        stage.setWidth(1024);
//        stage.setHeight(600);
//        stage.setScene(scene);
//        stage.setTitle("Program Kemitraan");
//        stage.show();
    }


}
