package com.client.blajar.Controller;

import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.stage.Stage;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Created by sukri on 01/12/16.
 */
public interface BootInitIalizer extends Initializable,ApplicationContextAware {

    public void initConstruct();
    public void stage(Stage primaryStage);
    public Node InitView();

}
