package com.client.blajar.Controller.User;

import com.client.blajar.Controller.BootInitIalizer;
import com.client.blajar.Controller.HomeController;
import com.client.blajar.Helper.Dlg;
import com.client.blajar.Model.Pelanggan;
import com.client.blajar.Model.User;
import com.client.blajar.Repository.UserRestRepository;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by sukri on 01/12/16.
 */
@Component
public class ListUserController implements BootInitIalizer {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    public TableView<User> tableview;
    public TableColumn<User, String> tablename;
    public TableColumn<User, String> tableid;
    public TableColumn<User, String> tableage;
    public TableColumn<User, String> tablesalary;
    public TextField salary;
    public TextField age;
    public TextField nama;

    public Button update;
    public Button delete;
    private ApplicationContext spring;

    @Autowired
    private HomeController homeController;
    @Autowired
    private FormUserController formUserController;
    @Autowired
    private UserRestRepository userRestRepository;


    @Override
    public void initConstruct() {
        reload_table();


    }

    private void reload_table() {
        tableview.getItems().clear();
        tableview.getItems().addAll(userRestRepository.findAllUser());
    }

    @Override
    public void stage(Stage primaryStage) {

    }

    @Override
    public Node InitView() {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/scene/user/list.fxml"));
            loader.setController(spring.getBean(this.getClass()));
            return loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tablename.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableage.setCellValueFactory(new PropertyValueFactory<>("age"));
        tablesalary.setCellValueFactory(new PropertyValueFactory<>("salary"));
        tableid.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableview.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends User> val, User oldValue, User newValue) -> {
            update.setDisable(newValue == null);
            update.setOnAction(event -> {
                doUpdate(event,newValue);
            });
            delete.setDisable(newValue == null);
            delete.setOnAction(event -> {
                doDelete(event,newValue);
            });

            nama.setText(newValue.getName());
            age.setText(newValue.getAge().toString());
            salary.setText(newValue.getSalary().toString());
        });
    }

    private void doDelete(ActionEvent event, User newValue) {
        if(Dlg.show("Peingatan","Apa anda yakin untuk menghapus data ini")){
            userRestRepository.deleteUser(newValue.getId().toString());
            tableview.getItems().remove(newValue);
        }
    }

    private void doUpdate(ActionEvent event, User newValue) {
        homeController.setCenterLayout(formUserController.InitView());
        formUserController.initConstruct(newValue);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.spring = applicationContext;

    }
    

    public void tambah(ActionEvent actionEvent) {
        homeController.setCenterLayout(formUserController.InitView());
        formUserController.initConstruct();

    }

    public void update(ActionEvent actionEvent) {

    }

    public void delete(ActionEvent actionEvent) {

    }
}
