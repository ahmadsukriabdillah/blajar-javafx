package com.client.blajar.Controller.User;

import com.client.blajar.Controller.BootInitIalizer;
import com.client.blajar.Controller.HomeController;
import com.client.blajar.Helper.Dlg;
import com.client.blajar.Model.User;
import com.client.blajar.Repository.UserRestRepository;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by sukri on 01/12/16.
 */
@Component
public class FormUserController implements BootInitIalizer {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @FXML
    private TextField nama;
    @FXML
    private TextField age;
    @FXML
    private TextField salary;


    private ApplicationContext spring;


    @Autowired
    private ListUserController listUserController;

    @Autowired
    private HomeController homeController;
    @Autowired
    private UserRestRepository userRestRepository;
    private User user;
    private boolean edited;


    public boolean isEdited() {
        return edited;
    }

    public void setEdited(boolean edited) {
        this.edited = edited;
    }

    @Override
    public void initConstruct()
    {
        clear();
        setEdited(false);
        nama.requestFocus();
    }
    public void initConstruct(User newValue) {
        clear();
        setEdited(true);
        user = newValue;
        nama.setText(newValue.getName());
        age.setText(newValue.getAge().toString());
        salary.setText(newValue.getSalary().toString());
        nama.requestFocus();


    }

    private void clear(){
        nama.clear();
        age.clear();
        salary.clear();

    }


    @Override
    public void stage(Stage primaryStage) {

    }

    @Override
    public Node InitView() {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/scene/user/form.fxml"));
            loader.setController(spring.getBean(this.getClass()));
            return loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.spring = applicationContext;

    }

    

    public void doSave(ActionEvent actionEvent) {
        if(isEdited()){
            if(Dlg.show("Konfirmasi","Apa yakin untuk mengubah data?")){
                ProgressBar progressBar = new ProgressBar();
                Task a = new Task() {
                    @Override
                    protected Object call() throws Exception {
                        user.setAge(Integer.parseInt(age.getText()));
                        user.setName(nama.getText());
                        user.setSalary(Integer.parseInt(salary.getText()));
                        userRestRepository.updateUser(user.getId().toString(),user);
                        return null;
                    }
                };
                a.setOnRunning(event -> {
                    progressBar.setVisible(true);
                });

                a.setOnSucceeded(event -> {
                    progressBar.setVisible(false);
                    doCance(actionEvent);
                });
                a.run();

            }

        }else{
            if(Dlg.show("Konfirmasi","Apa yakin untuk menambah data?")) {
                User a = new User();
                a.setAge(Integer.parseInt(age.getText()));
                a.setName(nama.getText());
                a.setSalary(Integer.parseInt(salary.getText()));
                userRestRepository.createUser(a);
                doCance(actionEvent);
            }


        }
    }

    public void doCance(ActionEvent actionEvent) {
        homeController.setCenterLayout(listUserController.InitView());
        listUserController.initConstruct();
    }
}
