package com.client.blajar;

/**
 * Created by sukri on 08/12/16.
 */
public class Constanta {

    public static final String SERVER = "http://localhost:8090/";
    public static final String REST_PELANGGAN = SERVER + "pelanggan/";
    public static final String AUTH = SERVER + "auth";
    public static final String REST_PEGAWAI = SERVER + "user/";
}
