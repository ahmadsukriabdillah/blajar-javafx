package com.client.blajar;

import com.client.blajar.Controller.HomeController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;


@SpringBootApplication
public class MainApplication extends Application {
	private static String[] argument;
	private ApplicationContext applicationContext = null;


	@Override
	public void start(Stage primaryStage) throws Exception {
		Task<Object> task = new Task<Object>() {
			@Override
			protected Object call() throws Exception {
				applicationContext = SpringApplication.run(MainApplication.class, argument);

				return null;
			}
		};


		task.setOnSucceeded(event -> {

			HomeController controller = applicationContext.getBean(HomeController.class);
			Parent parent = (Parent) controller.InitView();
            controller.initConstruct();
        	Scene scene = new Scene(parent);
			primaryStage.setResizable(false);
			primaryStage.setScene(scene);
//            primaryStage.setMaximized(true);
            primaryStage.setTitle("Program Kemitraan");
			primaryStage.show();

		});
		task.setOnFailed(e -> {
			System.exit(0);
			Platform.exit();
		});

		task.run();

	}
	
	public static void main(String[] args) {
		argument = args;
		launch(args);
	}
	
}
