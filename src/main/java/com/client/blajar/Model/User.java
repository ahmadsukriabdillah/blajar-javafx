package com.client.blajar.Model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("age")
	private Integer age;
	@JsonProperty("salary")
	private Integer salary;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public User() {
	}

	/**
	 *
	 * @param id
	 * @param age
	 * @param name
	 * @param salary
	 */
	public User(Integer id, String name, Integer age, Integer salary) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.salary = salary;
	}

	/**
	 *
	 * @return
	 * The id
	 */
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 * The id
	 */
	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 * The name
	 */
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 * The name
	 */
	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 * The age
	 */
	@JsonProperty("age")
	public Integer getAge() {
		return age;
	}

	/**
	 *
	 * @param age
	 * The age
	 */
	@JsonProperty("age")
	public void setAge(Integer age) {
		this.age = age;
	}

	/**
	 *
	 * @return
	 * The salary
	 */
	@JsonProperty("salary")
	public Integer getSalary() {
		return salary;
	}

	/**
	 *
	 * @param salary
	 * The salary
	 */
	@JsonProperty("salary")
	public void setSalary(Integer salary) {
		this.salary = salary;
	}


}
