package com.client.blajar.Model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by sukri on 07/12/16.
 */
public class Pelanggan {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("nama")
    private String nama;
    @JsonProperty("alamat")
    private String alamat;
    @JsonProperty("pekerjaan")
    private String pekerjaan;
    @JsonProperty("tempat")
    private String tempat;
    @JsonProperty("tgl_LAHIR")
    private String tglLAHIR;
    @JsonProperty("penghasilan")
    private Integer penghasilan;

    /**
     * No args constructor for use in serialization
     *
     */
    public Pelanggan() {
    }

    /**
     *
     * @param tempat
     * @param id
     * @param tglLAHIR
     * @param penghasilan
     * @param nama
     * @param alamat
     * @param pekerjaan
     */
    public Pelanggan(Integer id, String nama, String alamat, String pekerjaan, String tempat, String tglLAHIR, Integer penghasilan) {
        super();
        this.id = id;
        this.nama = nama;
        this.alamat = alamat;
        this.pekerjaan = pekerjaan;
        this.tempat = tempat;
        this.tglLAHIR = tglLAHIR;
        this.penghasilan = penghasilan;
    }

    /**
     *
     * @return
     * The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The nama
     */
    @JsonProperty("nama")
    public String getNama() {
        return nama;
    }

    /**
     *
     * @param nama
     * The nama
     */
    @JsonProperty("nama")
    public void setNama(String nama) {
        this.nama = nama;
    }

    /**
     *
     * @return
     * The alamat
     */
    @JsonProperty("alamat")
    public String getAlamat() {
        return alamat;
    }

    /**
     *
     * @param alamat
     * The alamat
     */
    @JsonProperty("alamat")
    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    /**
     *
     * @return
     * The pekerjaan
     */
    @JsonProperty("pekerjaan")
    public String getPekerjaan() {
        return pekerjaan;
    }

    /**
     *
     * @param pekerjaan
     * The pekerjaan
     */
    @JsonProperty("pekerjaan")
    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    /**
     *
     * @return
     * The tempat
     */
    @JsonProperty("tempat")
    public String getTempat() {
        return tempat;
    }

    /**
     *
     * @param tempat
     * The tempat
     */
    @JsonProperty("tempat")
    public void setTempat(String tempat) {
        this.tempat = tempat;
    }

    /**
     *
     * @return
     * The tglLAHIR
     */
    @JsonProperty("tgl_LAHIR")
    public String getTglLAHIR() {
        return tglLAHIR;
    }

    /**
     *
     * @param tglLAHIR
     * The tgl_LAHIR
     */
    @JsonProperty("tgl_LAHIR")
    public void setTglLAHIR(String tglLAHIR) {
        this.tglLAHIR = tglLAHIR;
    }

    @JsonProperty("penghasilan")
    public void setPenghasilan(Integer penghasilan) {
        this.penghasilan = penghasilan;
    }

    /**
     *
     * @return
     * The penghasilan
     */


    @JsonProperty("penghasilan")
    public Integer getPenghasilan() {
        return penghasilan;
    }
}
