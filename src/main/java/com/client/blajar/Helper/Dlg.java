package com.client.blajar.Helper;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * Created by sukri on 07/12/16.
 */
public class Dlg {

    public static boolean show(String title,String ket){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Konfirmasi");
        alert.setHeaderText(title);
        alert.setContentText(ket);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            return true;
        } else {
            return false;
            // ... user chose CANCEL or closed the dialog
        }
    }

    public static void eror(String title,String ket){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Erorr");
        alert.setHeaderText(title);
        alert.setContentText(ket);

        alert.showAndWait();
    }


}
